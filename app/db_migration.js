const { query } = require('./db')
const bcrypt = require('bcrypt')

// Create Table user
async function createTableUser() {
    await query(`
    CREATE TYPE role AS ENUM (
        'admin', 
        'employee'
    );`)

    await query(`
    CREATE TABLE IF NOT EXISTS users (
        id SERIAL,
        email VARCHAR NOT NULL,
        password VARCHAR NOT NULL,
        role ROLE NOT NULL,

        unique (email)
    )`)
}

async function createAdmin() {
    // Membuat hash password
    const passAdmin = await bcrypt.hash('password123', 12)
    const passEmployee = await bcrypt.hash('password321', 12)
    await query(`
    INSERT INTO users (
        email, 
        password, 
        role
    ) VALUES (
        'admin@binar.com',
        '${passAdmin}',
        'admin'
    ), (
        'employee@binar.com',
        '${passEmployee}',
        'employee'
    );`)
}

(async () => {
    await createTableUser()
    await createAdmin()
    console.info('SUCCESS MIGRATE TABLE!')
    process.exit(1)
})()