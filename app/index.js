const express = require("express")
const { query } = require("./db")

const passport = require("passport")
const LocalStrategy = require("passport-local")
const session = require("express-session")
const bcrypt = require("bcrypt")

const path = require("path")
const port = process.env.PORT || 8080

const {
    rootRequest,
    login,
    adminPage,
    employeePage,
    logout,
    loginPage,
    register,
    registerPage,
    deleteEmployee
} = require("./controllers")

/**
 * Menangkap promise error pada express, jika tidak menggunakan ini
 * tiap error promise yang terjadi akan memberhentikan semua request
 */
require("express-async-errors")

// Inisialisasi Express
const app = express()

// Agar Express bisa membaca JSON
app.use(express.json()) // Agar bisa pakai JSON
app.use(express.urlencoded()) // Agar bisa diakses di form

// Passport Local Strategy
passport.use(
    new LocalStrategy(async function (username, password, done) {
        // Mengecek apakah ada user dengan email tersebut
        const getUser = await query(`SELECT * FROM users WHERE email = '${username}'`)
        const user = getUser.rows[0]

        // Kalau tidak ada, kirim pesan "Password atau username salah!"
        if (!user) {
            return done(null, false, { message: "Password atau username salah!" })
        }

        // Megecek apakah password yang dituliskan user sudah sesuai atai tidak
        if (!(await bcrypt.compare(password, user.password))) {
            return done(null, false, { message: "Password atau username salah!" })
        }

        return done(null, user)
    })
)

// Membuat inject req.session object dengan user
passport.serializeUser((user, done) => {
    done(null, user)
})

// Membuat inject req object dengan user
passport.deserializeUser((user, done) => {
    done(null, user)
})

// Inisialisasi penggunaan passport
app.use(
    session({ 
        secret: "yang=penting=aman" 
    })
)
app.use(passport.initialize())
app.use(
    passport.session({
        secret: "yang=penting=aman",
        resave: false,
        saveUninitialized: true,
        cookie: { maxAge: 60 * 60 * 1000 } // 1 Jam
    })
)

// Menggunakan template engine, dengan tipe ejs
app.set("view engine", "ejs")
app.set("views", path.join(__dirname, "views"))
app.engine("ejs", require("ejs").__express)

// Router
app.get("/", (req, res) => rootRequest(req, res))
app.get("/login", (req, res) => loginPage(req, res))
app.post(
    "/login",
    passport.authenticate("local", { failureRedirect: "/" }),
    (req, res) => login(req, res)
)
app.get("/register", (req, res) => registerPage(req, res))
app.post("/register", (req, res) => register(req, res))

app.get("/admin", (req, res) => adminPage(req, res))
app.get("/admin/delete/:id", (req, res) => deleteEmployee(req, res))
app.get("/employee", (req, res) => employeePage(req, res))
app.post("/logout", (req, res) => logout(req, res))

app.listen(port, () => {
    console.info(`Aplikasi berjalan di port: http://localhost:${port}`)
})