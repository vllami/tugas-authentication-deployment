const db = require("../db")
const bcrypt = require("bcrypt")

function rootRequest(req, res) {
    res.render("home_page.ejs")
}

function loginPage(req, res) {
    if (req.isAuthenticated()) {
        if (req.user.role === "admin") {
            return res.redirect("/admin")
        } else {
            return res.redirect("/employee")
        }
    } else {
        res.render("login_page.ejs")
    }
}

function registerPage(req, res) {
    if (req.isAuthenticated()) {
        if (req.user.role === "admin") {
            return res.redirect("/admin")
        } else {
            return res.redirect("/employee")
        }
    } else {
        res.render("register_page.ejs")
    }
}

async function login(req, res) {
    const user = req.user
    if (user.role === "admin") {
        return res.redirect("/admin")
    } else {
        return res.redirect("/employee")
    }
}

async function register(req, res) {
    const { email, password } = req.body
    const hashPassword = await bcrypt.hash(password, 10)
    const user = await db.query(
        `INSERT INTO users (
            email, 
            password, 
            role
        ) VALUES (
            '${email}', 
            '${hashPassword}', 
            'employee'
        )`
    )
    if (user.rowCount === 1) {
        res.redirect("/login")
    } else {
        res.redirect("/register")
    }
}

async function adminPage(req, res) {
    if (req.isAuthenticated()) {
        if (req.user.role === "admin") {
            const result = await db.query(`SELECT * FROM users WHERE role = 'employee'`)
            users = result.rows
            res.render("admin_page.ejs", { users })
        } else {
            res.render("limited_access.ejs")
        }
    } else {
        res.render("forbidden.ejs")
    }
}

function employeePage(req, res) {
    if (req.isAuthenticated()) {
        res.render("employee_page.ejs")
    } else {
        res.render("forbidden.ejs")
    }
}

function logout(req, res) {
    req.logout()
    res.redirect("/")
}

async function deleteEmployee(req, res) {
    const id = req.params.id
    await db.query(`DELETE FROM users WHERE id = ${id}`)
    res.redirect("/admin")
}

module.exports = {
    rootRequest,
    loginPage,
    registerPage,
    login,
    register,
    adminPage,
    employeePage,
    logout,
    deleteEmployee
}